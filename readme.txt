Zadanie wykonywane w ramach kursu Java.

Stworzyć aplikację java fx BABY DICTIONARY tłumaczącą gaworzenie na „ludzką mowę” ;)
W aplikacji można wpisać gaworzynę (np. gugu) i przetłumaczyć na podstawie słownika zapisanego w
pliku tekstowym (format dowolny) wynikiem czego powinniśmy otrzymać w przeglądarce lub kliencie
słowo… na przykład „jeść”.
Jeżeli słowa nie ma w słowniku to metoda zwróci odpowiednią informację.
Rozwinięcie:
Stworzyć prostą formatkę do dodawnia nowych gaworzyn. Formatka powinna domyślnie być ukryta i
uwidaczniać się po naciśnięciu przycisku. Formatka może być widoczna na tym samym scene graph
(wersja łatwiejsza) albo w nowym okienku (wersja trudniejsza).






Plik dictionary.csv powinien znajdować się w obecnym katalogu roboczym.
