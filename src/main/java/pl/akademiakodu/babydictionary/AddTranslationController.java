package pl.akademiakodu.babydictionary;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;


public class AddTranslationController {

    @FXML
    private TextField textFieldEntry;

    @FXML
    private TextField textFieldTranslation;

    private DictionaryManager dictionaryManager;

    void setDictionaryManager(DictionaryManager dictionaryManager) {
        this.dictionaryManager = dictionaryManager;
    }

    public void buttonAddOnAction() throws IOException {
        if(!dictionaryManager.hasEntry(textFieldEntry.getText())){
            dictionaryManager.addEntry(textFieldEntry.getText(), textFieldTranslation.getText());
            Stage stage = (Stage) textFieldTranslation.getScene().getWindow();
            stage.close();
        }else{
            Stage stage = (Stage) textFieldTranslation.getScene().getWindow();
            stage.close();
        }
    }
}
