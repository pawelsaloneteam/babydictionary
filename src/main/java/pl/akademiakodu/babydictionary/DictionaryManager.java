package pl.akademiakodu.babydictionary;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;
import java.util.Map;

class DictionaryManager {
    private Path path;
    private Map<String, String> dictionary;

    DictionaryManager(){
        dictionary = new HashMap<>();

        try {
            String[] pathString = getClass().getProtectionDomain().getCodeSource().getLocation().getPath().split("/");
            path = Paths.get(pathString[1], "\\");
            for(int i = 2; i < pathString.length - 1; i++){
                path = path.resolve(pathString[i]);
            }
            path = path.resolve("./dictionary.csv");
            System.out.println(path.toString());

            Files.readAllLines(path).forEach(line -> {
                String[] items = line.split(";");
                dictionary.put(items[0], items[1]);
            });

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Loading data from dictionary file failed");
        }
    }


    boolean hasEntry(String entry) {
        return dictionary.containsKey(entry);
    }

    String getTranslation(String entry) {
        return dictionary.get(entry);
    }

    void addEntry(String entryText, String translationText) throws IOException {
        dictionary.put(entryText, translationText);
        Files.write(path, (entryText + ";" + translationText+ "\n").getBytes(), StandardOpenOption.APPEND);
    }
}
