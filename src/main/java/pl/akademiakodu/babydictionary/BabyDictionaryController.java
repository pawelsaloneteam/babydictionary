package pl.akademiakodu.babydictionary;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;

public class BabyDictionaryController {

    @FXML
    private TextField textFieldTranslation;

    @FXML
    private TextField textFieldGaworzyna;

    private DictionaryManager dictionaryManager;

    @FXML
    public void initialize(){
        dictionaryManager = new DictionaryManager();
    }


    @FXML
    public void buttonAddTranslationOnAction() throws IOException {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("views/AddTranslation.fxml"));
        Parent root = loader.load();
        AddTranslationController controllerAddTransaction = loader.getController();
        controllerAddTransaction.setDictionaryManager(dictionaryManager);

        Scene scene = new Scene(root);
        Stage stageAddTranslation = new Stage();
        stageAddTranslation.setScene(scene);
        stageAddTranslation.show();
    }

    @FXML
    public void buttonTranslateOnAction() {
        String entry = textFieldGaworzyna.getText();
        if(dictionaryManager.hasEntry(entry)){
            textFieldTranslation.setText(dictionaryManager.getTranslation(entry));
        }else{
            textFieldTranslation.setText("Hasło nieznane.");

        }
    }
}
